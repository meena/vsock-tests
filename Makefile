# SPDX-License-Identifier: GPL-2.0-only
all: test
test: vsock_test
vsock_test: vsock_test.o timeout.o control.o util.o

CFLAGS += -g -O2 -Werror -Wall -I.  -Wno-pointer-sign -fno-strict-overflow -fno-strict-aliasing -fno-common -MMD -U_FORTIFY_SOURCE -D_GNU_SOURCE
.PHONY: all test clean
clean:
	${RM} *.o *.d vsock_test
-include *.d
