# AF_VSOCK test suite

These tests exercise virtio_socket host<->guest sockets for KVM, but should in theory work for VMware and Hyper-V, too.

The following tests are available:

  * vsock_test - core AF_VSOCK socket functionality

The following prerequisite steps are not automated and must be performed prior to running tests:

1. On the FreeBSD Guest, build the [FreeBSD kernel module](https://codeberg.org/meena/freebsd-src/src/branch/vsock/sys/dev/virtio/socket), install the headers (`sys/socket.h`, `sys/vm_sockets.h`)
2. Build these tests with GNU make
3. On the Linux Host, build and install this [full Linux vsock test suite](https://github.com/torvalds/linux/tree/master/tools/testing/vsock)
4. On the FreeBSD Guest, install the FreeBSD kernel module and these tests inside the guest.
5. `kldload -v virtio_socket` and ensure that it shows up as loaded `kldstat(8)`, and shows diagnostics in `dmesg`

Invoke test binaries in both directions as follows:

```
# host=server, guest=client
(host)# ./vsock_test --mode=server \
                      --control-port=1234 \
                      --peer-cid=3
(guest)# ./vsock_test --mode=client \
                      --control-host=$HOST_IP \
                      --control-port=1234 \
                      --peer-cid=2
```

and

```
# host=client, guest=server
(guest)# ./vsock_test --mode=server \
                      --control-port=1234 \
                      --peer-cid=2
(host)# ./vsock_test  --mode=client \
                      --control-port=$GUEST_IP \
                      --control-port=1234 \
                      --peer-cid=3
```

## vsock_perf utility

The 'vsock_perf' is, as of yet, not provided on FreeBSD.
